package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.shop24hapi.model.CProducts;

public interface IProductsRepository extends JpaRepository<CProducts, Integer>{

    List<CProducts> findByProductLineIdAndDisplay(Integer productLineId, String display, Pageable pageWithElements);

    List<CProducts> findByDisplay(String display, Pageable pageWithElements);

    List<CProducts> findByProductVendor(String productVendor, Pageable pageWithElements);

    @Query(value = "SELECT * FROM `products` WHERE product_vendor LIKE ?1 "
    + " AND product_line_id LIKE ?2 "
    + " AND discount_price BETWEEN ?3 AND ?4"
    + " AND quantity_in_stock > ?5 AND product_name LIKE %?6% AND  display = 'show' ", nativeQuery = true)
    List<CProducts> findByProperties1(
    @Param("productVendor") String productVendor,
    @Param("productLineId") String productLineId,
    @Param("price1") String price1, @Param("price2") String price2,
    @Param("quantityInStock") String quantityInStock,
    @Param("productName") String productName, Pageable pageWithElements);

    @Query(value = "SELECT * FROM `products` WHERE product_vendor LIKE ?1 "
    + " AND product_line_id LIKE ?2 "
    + " AND discount_price BETWEEN ?3 AND ?4"
    + " AND quantity_in_stock > ?5 AND product_name LIKE %?6% AND display = 'show' AND discount_price < buy_price", nativeQuery = true)
    List<CProducts> findByProperties2(
    @Param("productVendor") String productVendor,
    @Param("productLineId") String productLineId,
    @Param("price1") String price1, @Param("price2") String price2,
    @Param("quantityInStock") String quantityInStock,
    @Param("productName") String productName, Pageable pageWithElements);

    @Query(value = "SELECT * FROM `products` WHERE product_name LIKE %?1% AND product_line_id LIKE ?2 AND display = 'show'", nativeQuery = true)
    List<CProducts> findByNameOfProduct(@Param("productName") String productName,
    @Param("productLineId") String productLineId, Pageable pageWithElements);
}
