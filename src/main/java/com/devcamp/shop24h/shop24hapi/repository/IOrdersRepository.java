package com.devcamp.shop24h.shop24hapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.shop24hapi.model.COrders;

public interface IOrdersRepository extends JpaRepository<COrders, Integer>{
    List<COrders> findByCustomerId(Integer customerId);
    
    List<COrders> findByStatusAndCustomerId(String status, Integer customerId);

    List<COrders> findByStatus(String status);

    Optional<COrders> findByIdAndCustomerId(Integer id, Integer customerId);

    @Query(value = "SELECT * FROM orders WHERE status LIKE :status AND order_date BETWEEN :Date1 AND :Date2 ORDER BY status",nativeQuery = true)
    List<COrders> findByRequest(@Param("status") String status,@Param("Date1") String Date1,@Param("Date2") String Date2);

    @Query(value = "SELECT * FROM orders WHERE order_date BETWEEN :date1 AND :date2 ORDER BY status",nativeQuery = true)
    List<COrders> findByDateRangeOfOrder(@Param("date1") String date1,@Param("date2") String date2);

    @Query(value = "SELECT COUNT(customer_id),customer_id AS customerNumber FROM `orders` GROUP BY customer_id ORDER BY COUNT(customer_id) DESC",nativeQuery = true)
    List<?> findByQuantityOrder();
}
