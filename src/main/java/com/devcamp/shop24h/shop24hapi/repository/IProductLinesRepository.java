package com.devcamp.shop24h.shop24hapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.shop24hapi.model.CProductLines;

public interface IProductLinesRepository extends JpaRepository<CProductLines, Integer>{
    
}
