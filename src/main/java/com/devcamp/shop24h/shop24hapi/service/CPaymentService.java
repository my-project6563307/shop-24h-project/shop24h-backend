package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.CPayments;
import com.devcamp.shop24h.shop24hapi.payload.response.MessageResponse;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.repository.IPaymentsRepository;
import com.devcamp.shop24h.shop24hapi.security.services.UserDetailsServiceImpl;

@Service
public class CPaymentService {
    @Autowired
    private IPaymentsRepository paymentsRepository;

    @Autowired
    private ICustomerRepository customerRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    // ================ MỤC DÀNH CHO QUẢN TRỊ VIÊN (MANAGER ROLE) =====================

    //lấy toàn bộ danh sách hoá đơn của shop24h
    public ResponseEntity<List<CPayments>> getAllPayments(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CPayments> paymentList = new ArrayList<>();
            paymentsRepository.findAll(pageWithElements).forEach(paymentList::add);

            return new ResponseEntity<>(paymentList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy hoá đơn qua id hoá đơn
    public ResponseEntity<CPayments> getPaymentById (Integer id){
        try {
            Optional<CPayments> paymentData = paymentsRepository.findById(id);
            if(paymentData.isPresent()){

                CPayments officeFound = paymentData.get();

                return new ResponseEntity<>(officeFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Tạo hoá đơn cho khách hàng
    public ResponseEntity<Object> createPayment(CPayments pPayment, Integer customerId){
        try {
                CPayments newPayment =  new CPayments();
                Optional<CCustomers> customerData = customerRepository.findById(customerId);

                if(customerData.isPresent()){
                    // Cập nhật tổng tiền chi tiêu cho khách hàng
                    CCustomers customerFound = customerData.get();
                    customerFound.setTotalPayment(customerFound.getTotalPayment() + pPayment.getAmount());
                    customerRepository.saveAndFlush(customerFound);
                    // Tạo hoá đơn
                    newPayment.setPaymentDate(pPayment.getPaymentDate());
                    newPayment.setAmount(pPayment.getAmount());
                    newPayment.setCustomer(customerFound);
                    return new ResponseEntity<>(paymentsRepository.saveAndFlush(newPayment), HttpStatus.OK);
                }

                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    // Cập nhật hoá đơn cho khách hàng
    public ResponseEntity<Object> updatePayment(Integer paymentId, CPayments pPayment, Integer customerId){
        try {
                Optional<CPayments> paymentData =  paymentsRepository.findById(paymentId);
                Optional<CCustomers> customerData = customerRepository.findById(customerId);

                if(customerData.isPresent() && paymentData.isPresent()){

                    CPayments updatePayment = paymentData.get();
                    updatePayment.setPaymentDate(pPayment.getPaymentDate());
                    updatePayment.setAmount(pPayment.getAmount());
                    updatePayment.setCustomer(customerData.get());
                    return new ResponseEntity<>(paymentsRepository.saveAndFlush(updatePayment), HttpStatus.OK);
                }

                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy tổng tiền trong giai đoạn
    public ResponseEntity<?> getAnualAmount(String Date1, String Date2){
        try {
            Long aMount = paymentsRepository.findByRequest(Date1, Date2);

            return new ResponseEntity<>(aMount, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // =================== PHẦN DÀNH CHO KHÁCH HÀNG =====================
    // Lấy danh sách hoá đơn qua request token khách hàng
    public ResponseEntity<?> getPaymentByRequest(HttpServletRequest request){
        try {
            CCustomers customerFound = userDetailsServiceImpl.whoami(request);

            List<CPayments> paymentList = paymentsRepository.findByCustomerId(customerFound.getId());
            if (paymentList.size() > 0){
                return new ResponseEntity<>(paymentList,HttpStatus.OK);
            }
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: No payments found!"));

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
