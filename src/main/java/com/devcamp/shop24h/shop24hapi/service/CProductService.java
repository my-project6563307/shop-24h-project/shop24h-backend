package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CProductLines;
import com.devcamp.shop24h.shop24hapi.model.CProducts;
import com.devcamp.shop24h.shop24hapi.repository.IProductLinesRepository;
import com.devcamp.shop24h.shop24hapi.repository.IProductsRepository;

@Service
public class CProductService {
    @Autowired
    private IProductLinesRepository productLinesRepository;

    @Autowired
    private IProductsRepository productsRepository;
    
    // =========================== PUBLIC ==================================

    // Lấy danh sách sản phẩm được hiển thị
    public ResponseEntity<List<CProducts>> getAllProductsByDisplay(String page, String size, String display){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CProducts> productList = new ArrayList<>();
            productsRepository.findByDisplay(display,pageWithElements).forEach(productList::add);
            
            return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy sản phẩm qua id
    public ResponseEntity<CProducts> getProductById (Integer id){
        try {
            Optional<CProducts> productData = productsRepository.findById(id);

            if(productData.isPresent()){

                CProducts productFound = productData.get();

                return new ResponseEntity<>(productFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy sản phẩm qua product line 
    public ResponseEntity<List<CProducts>> getProductByProductLineId (Integer productLineId, String display, String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CProducts> productList = productsRepository.findByProductLineIdAndDisplay(productLineId, display, pageWithElements);

            if(productList.size() > 0){;
                return new ResponseEntity<>(productList, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Lấy sản phẩm qua product vendor
    public ResponseEntity<List<CProducts>> getProductByProductVendor (String productVendor, String size, String page){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CProducts> productList = productsRepository.findByProductVendor(productVendor, pageWithElements);

            if(productList.size() > 0){;
                return new ResponseEntity<>(productList, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // ================= MỤC DÀNH CHO QUẢN LÍ (MANAGER) =================
    // Lấy danh sách sản phẩm được hiển thị
    public ResponseEntity<List<CProducts>> getAllProducts(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CProducts> productList = new ArrayList<>();
            productsRepository.findAll(pageWithElements).forEach(productList::add);
            
            return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Cập nhật thông tin sản phẩm qua id
    public ResponseEntity<Object> updateProductById(Integer productLineId, Integer id, CProducts product){
        try {
            Optional<CProducts> productData = productsRepository.findById(id);

            Optional<CProductLines> productLineData = productLinesRepository.findById(productLineId);

            if (productData.isPresent()){

                CProducts productFound =  productData.get();

                productFound.setProductLine(productLineData.get());
                productFound.setProductCode(product.getProductCode());
                productFound.setProductName(product.getProductName());
                productFound.setPhoto1(product.getPhoto1());
                productFound.setPhoto2(product.getPhoto2());
                productFound.setPhoto3(product.getPhoto3());
                productFound.setPhoto4(product.getPhoto4());
                productFound.setDiscountPrice(product.getDiscountPrice());
                productFound.setProductDescription(product.getProductDescription());
                productFound.setProductScale(product.getProductScale());
                productFound.setQuantityInStock(product.getQuantityInStock());
                productFound.setProductVendor(product.getProductVendor());
                productFound.setBuyPrice(product.getBuyPrice());
                productFound.setDisplay(product.getDisplay());

                return new ResponseEntity<>(productsRepository.saveAndFlush(productFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Thêm mới sản phẩm
    public ResponseEntity<Object> createProduct(Integer productLineId,CProducts product){
        try {
            
                Optional<CProductLines> productLineData = productLinesRepository.findById(productLineId);

                CProducts newProduct =  new CProducts();

                newProduct.setProductLine(productLineData.get());
                newProduct.setProductCode(product.getProductCode());
                newProduct.setProductName(product.getProductName());
                newProduct.setPhoto1(product.getPhoto1());
                newProduct.setPhoto2(product.getPhoto2());
                newProduct.setPhoto3(product.getPhoto3());
                newProduct.setPhoto4(product.getPhoto4());
                newProduct.setDiscountPrice(product.getDiscountPrice());
                newProduct.setProductDescription(product.getProductDescription());
                newProduct.setProductScale(product.getProductScale());
                newProduct.setQuantityInStock(product.getQuantityInStock());
                newProduct.setProductVendor(product.getProductVendor());
                newProduct.setBuyPrice(product.getBuyPrice());
                newProduct.setDisplay(product.getDisplay());

                return new ResponseEntity<>(productsRepository.saveAndFlush(newProduct), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá thông tin 1 sản phẩm qua id
    public ResponseEntity<Object> deleteProductById(Integer id){
        try {
            Optional<CProducts> productData = productsRepository.findById(id);

            if(productData.isPresent()){
                productsRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy số lượng sản phẩm
    public ResponseEntity<Object> getNumberProduct(){
        try {
            List<CProducts> listProduct = productsRepository.findAll();

            return new ResponseEntity<>(listProduct.size(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
