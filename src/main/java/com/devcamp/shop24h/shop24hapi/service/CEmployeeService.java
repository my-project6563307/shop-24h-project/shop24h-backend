package com.devcamp.shop24h.shop24hapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.Cemployees;
import com.devcamp.shop24h.shop24hapi.repository.IEmployeesRepository;

@Service
public class CEmployeeService {
    @Autowired
    private IEmployeesRepository employeesRepository;
    // ================= MỤC DÀNH CHO MANAGER ==================

    //lấy toàn bộ danh sách nhân viên của shop
    public ResponseEntity<List<Cemployees>> getAllEmployees(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<Cemployees> employeeList = new ArrayList<>();
            employeesRepository.findAll(pageWithElements).forEach(employeeList::add);

            return new ResponseEntity<>(employeeList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy thông tin nhân viên qua id nhân viên
    public ResponseEntity<Cemployees> getEmployeeById (Integer id){
        try {
            Optional<Cemployees> employeeData = employeesRepository.findById(id);
            if(employeeData.isPresent()){

                Cemployees employeeFound = employeeData.get();

                return new ResponseEntity<>(employeeFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Cập nhật thông tin nhân viên qua id
    public ResponseEntity<Object> updateEmployeeById(Integer id, Cemployees pEmployee){
        try {
            Optional<Cemployees> employeeData = employeesRepository.findById(id);

            if (employeeData.isPresent()){

                Cemployees employeeFound =  employeeData.get();

                employeeFound.setFirstName(pEmployee.getFirstName());
                employeeFound.setLastName(pEmployee.getLastName());
                employeeFound.setExtension(pEmployee.getExtension());
                employeeFound.setEmail(pEmployee.getEmail());
                employeeFound.setPhoto(pEmployee.getPhoto());
                employeeFound.setOfficeCode(pEmployee.getOfficeCode());
                employeeFound.setReportTo(pEmployee.getReportTo());
                employeeFound.setJobTitle(pEmployee.getJobTitle());

                return new ResponseEntity<>(employeesRepository.saveAndFlush(employeeFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Thêm mới nhân viên 
    public ResponseEntity<Object> createEmployee(Cemployees pEmployee){
        try {
            
                Cemployees newEmployee =  new Cemployees();

                newEmployee.setFirstName(pEmployee.getFirstName());
                newEmployee.setLastName(pEmployee.getLastName());
                newEmployee.setExtension(pEmployee.getExtension());
                newEmployee.setEmail(pEmployee.getEmail());
                newEmployee.setPhoto(pEmployee.getPhoto());
                newEmployee.setOfficeCode(pEmployee.getOfficeCode());
                newEmployee.setReportTo(pEmployee.getReportTo());
                newEmployee.setJobTitle(pEmployee.getJobTitle());

                return new ResponseEntity<>(employeesRepository.saveAndFlush(newEmployee), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá thông tin 1 nhân viên qua id 
    public ResponseEntity<Object> deleteEmployeesById(Integer id){
        try {
            Optional<Cemployees> employeeData = employeesRepository.findById(id);

            if(employeeData.isPresent()){
                employeesRepository.delete(employeeData.get());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
