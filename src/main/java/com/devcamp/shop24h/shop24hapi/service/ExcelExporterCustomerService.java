package com.devcamp.shop24h.shop24hapi.service;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;


@Service
public class ExcelExporterCustomerService {
    private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<CCustomers> customers;

	/**
	 * Constructor khởi tạo server export danh sách Customer 
	 * @param customers
	 */
    
     public ExcelExporterCustomerService(List<CCustomers> customers) {
        this.customers = customers;
        this.workbook = new XSSFWorkbook();
    }

    /**
	 * Tạo các ô cho excel file.
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCells(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

    /**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
		this.sheet = workbook.createSheet("Customers");
		Row row = this.sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCells(row, 0, "Customer ID", style);
		createCells(row, 1, "Name", style);
		createCells(row, 2, "Address", style);
		createCells(row, 3, "City", style);
		createCells(row, 4, "Customer Phone", style);
		createCells(row, 5, "Email", style);
		createCells(row, 6, "username", style);
		createCells(row, 7, "active", style);
		createCells(row, 8, "Total payment", style);
	}

    /**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (CCustomers customer : this.customers) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCells(row, columnCount++, customer.getId(), style);
			createCells(row, columnCount++, customer.getFirstName() + " " + customer.getLastName(), style);
			createCells(row, columnCount++, customer.getAddress(), style);
			createCells(row, columnCount++, customer.getCity(), style);
			createCells(row, columnCount++, customer.getPhoneNumber(), style);
			createCells(row, columnCount++, customer.getEmail(), style);	
			createCells(row, columnCount++, customer.getUsername(), style);	
			createCells(row, columnCount++, customer.isActive(), style);	
			createCells(row, columnCount++, customer.getTotalPayment().toString(), style);	

		}
	}

    /**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}

