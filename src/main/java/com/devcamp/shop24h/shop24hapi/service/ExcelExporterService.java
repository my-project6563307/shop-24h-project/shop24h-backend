package com.devcamp.shop24h.shop24hapi.service;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import com.devcamp.shop24h.shop24hapi.model.COrders;


@Service
public class ExcelExporterService {
    private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<COrders> orders;

	/**
	 * Constructor khởi tạo server export danh sách Order 
	 * @param orders
	 */
    
    public ExcelExporterService(List<COrders> orders) {
        this.orders = orders;
        this.workbook = new XSSFWorkbook();
    }

    /**
	 * Tạo các ô cho excel file.
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCells(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

    /**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
		this.sheet = workbook.createSheet("Order");
		Row row = this.sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCells(row, 0, "Order ID", style);
		createCells(row, 1, "Order Date", style);
		createCells(row, 2, "Status", style);
		createCells(row, 3, "Delivery Addresss", style);
		createCells(row, 4, "Customer Phone", style);
		createCells(row, 5, "Comments", style);
		createCells(row, 6, "Delivery Date", style);
		createCells(row, 7, "Shipped date", style);
	}

    /**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (COrders order : this.orders) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCells(row, columnCount++, order.getId(), style);
			createCells(row, columnCount++, order.getOrderDate().toString(), style);
			createCells(row, columnCount++, order.getStatus(), style);
			createCells(row, columnCount++, order.getDeliveryAddress(), style);
			createCells(row, columnCount++, order.getCustomer().getPhoneNumber(), style);
			createCells(row, columnCount++, order.getComments(), style);
            if(order.getRequiredDate() != null){
                createCells(row, columnCount++, order.getRequiredDate().toString(), style);
            }
            else {
                createCells(row, columnCount++, "", style);
            }
            if(order.getShippedDate() != null){
                createCells(row, columnCount++, order.getShippedDate().toString(), style);
            }
            else {
                createCells(row, columnCount++, "", style);
            }
			

		}
	}

    /**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
