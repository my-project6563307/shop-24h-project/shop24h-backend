package com.devcamp.shop24h.shop24hapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.devcamp.shop24h.shop24hapi.property.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class Shop24hApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Shop24hApiApplication.class, args);
	}

}
