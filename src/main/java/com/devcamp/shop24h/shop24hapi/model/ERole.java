package com.devcamp.shop24h.shop24hapi.model;

public enum ERole {
    ROLE_CUSTOMER,
    ROLE_MANAGER
}
