package com.devcamp.shop24h.shop24hapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "order_details")
public class COrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "quatity_order")
    private Integer quantity_order;

    @Column(name = "price_each")
    private Long priceEach; 

    @ManyToOne
    @JoinColumn(name = "order_id")
    private COrders order;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private CProducts product;
}
