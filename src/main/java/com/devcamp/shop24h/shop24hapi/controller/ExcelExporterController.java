package com.devcamp.shop24h.shop24hapi.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.model.COrders;
import com.devcamp.shop24h.shop24hapi.repository.ICustomerRepository;
import com.devcamp.shop24h.shop24hapi.repository.IOrdersRepository;
import com.devcamp.shop24h.shop24hapi.service.ExcelExporterCustomerService;
import com.devcamp.shop24h.shop24hapi.service.ExcelExporterService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class ExcelExporterController {
    @Autowired
    private IOrdersRepository ordersRepository;

    @Autowired
    private ICustomerRepository customerRepository;
    // Xuất file excel order
    @GetMapping("api/noauth/export/order/excel")
	public void exportOrderToExcel(@RequestParam String status, @RequestParam String Date1,@RequestParam String Date2, HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=Orders_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<COrders> order = new ArrayList<>();

        if(status.equals("all")){
            status = "%";
        }
        ordersRepository.findByRequest(status, Date1, Date2).forEach(order::add);

		ExcelExporterService excelExporter = new ExcelExporterService(order);
		excelExporter.export(response);
	}
    // Xuất file excel customer
    @GetMapping("api/noauth/export/customer/excel")
	public void exportCustomerToExcel(@RequestParam String payment1,@RequestParam String payment2, HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=Customers_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<CCustomers> customerList = new ArrayList<>();

        customerRepository.findByLevelOfPayment(payment1, payment2).forEach(customerList::add);

		ExcelExporterCustomerService excelcustomerExporter = new ExcelExporterCustomerService(customerList);
		excelcustomerExporter.export(response);
	}

}
