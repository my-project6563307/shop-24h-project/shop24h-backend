package com.devcamp.shop24h.shop24hapi.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.service.CFavouriteProductService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class CFavouriteProductController {
    @Autowired
    private CFavouriteProductService favouriteProductService;
    //================ CUSTOMER=================
    // Lấy danh sách sản phẩm ưa thích theo khách hàng
    @GetMapping("wishlists")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<Object> getFavouriteProduct(HttpServletRequest request){
        return favouriteProductService.getFavouriteProduct(request);
    }

    // Tạo sản phẩm ưa thích theo khách hàng và sản phẩm
    @PostMapping("wishlists")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<Object> createFavouriteProduct(HttpServletRequest request,@RequestParam Integer productId){
        return favouriteProductService.createFavouriteProduct(request, productId);
    }

    // Xoá sản phẩm yêu thích theo id
    @DeleteMapping("wishlists")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('MANAGER')")
    public ResponseEntity<Object> deleteFavouriteProduct(HttpServletRequest request,@RequestParam Integer productId){
        return favouriteProductService.deleteFavouriteProduct(request, productId);
    }


}
