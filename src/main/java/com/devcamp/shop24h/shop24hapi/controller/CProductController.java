package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CProducts;
import com.devcamp.shop24h.shop24hapi.repository.IProductsRepository;
import com.devcamp.shop24h.shop24hapi.service.CProductService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class CProductController {
    @Autowired
    private CProductService productService;

    @Autowired
    private IProductsRepository productsRepository;
    
    // =========================== PUBLIC ==================================
    // Lấy danh sách sản phẩm được hiển thị
    @GetMapping("api/noauth/products")
    public ResponseEntity<List<CProducts>> getAllProductsByDisplay(@RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "12") String size, String display){
        display = "show";
        return  productService.getAllProductsByDisplay(page, size, display);
    }
    // Lấy sản phẩm qua id
    @GetMapping("api/noauth/products/detail/{id}")
    public ResponseEntity<CProducts> getProductById (@PathVariable Integer id){
        return  productService.getProductById(id);
    }

    // Lấy sản phẩm qua product line 
    @GetMapping("api/noauth/products/product-line/{productLineId}")
    public ResponseEntity<List<CProducts>> getProductByProductLineId(@PathVariable Integer productLineId,
        String display, 
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "12") String size){
            display = "show";
        return productService.getProductByProductLineId(productLineId,display,page,size);
    }

    // Lấy sản phẩm qua product vendor
    @GetMapping("api/noauth/products/product-vendor/{productVendor}")
    public ResponseEntity<List<CProducts>> getProductByProductVendor (@PathVariable String productVendor,
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "12") String size){
        return productService.getProductByProductVendor(productVendor,page, size);
    }

    // Tìm sản phẩm qua các thuộc tính
    @GetMapping("api/noauth/products/product-props/product-vendor/{productVendor}/product-line-id/{productLineId}/"
    + "price1/{price1}/price2/{price2}/quantity-in-stock/{quantityInStock}/onsale/{onSale}/product-name/{productName}")
    List<CProducts> findByProperties(
    @PathVariable String productVendor,
    @PathVariable String productLineId,
    @PathVariable String price1, @PathVariable String price2,
    @PathVariable String quantityInStock,
    @PathVariable String onSale,
    @PathVariable String productName,
    @RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "12") String size){

        Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

        if(productVendor.equals("all")){
            productVendor = "%";
        }
        if (productLineId.equals("all")){
            productLineId = "%";
        }
        if(productName.equals("all")){
            productName = "%";
        }
        if(onSale.equals("1")){
            List<CProducts> filterProduct = productsRepository.findByProperties2(productVendor, productLineId, price1, price2,quantityInStock,productName,pageWithElements);
            return filterProduct;
        }
        else {
            List<CProducts> listProducts = productsRepository.findByProperties1(productVendor, productLineId,price1,price2,quantityInStock,productName,pageWithElements);
            return listProducts;
        }
    }

    // Tìm sản phẩm qua tên sản phẩm
    @GetMapping("api/noauth/products/product-name/{productName}/product-line-id/{productLineId}")
    public ResponseEntity<List<CProducts>> findProductByNameOfProduct (@PathVariable String productName,
        @PathVariable String productLineId,
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "12") String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            if (productLineId.equals("all")){
                productLineId = "%";
            }
            if(productName.equals("all")){
                productName = "%";
            }
            List<CProducts> productList = productsRepository.findByNameOfProduct(productName,productLineId,pageWithElements);

        return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // ================= MỤC DÀNH CHO QUẢN LÍ (MANAGER) =================

    // Lấy danh sách sản phẩm
    @GetMapping("products")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<List<CProducts>> getAllProducts(
        @RequestParam(defaultValue = "0") String page,@RequestParam(defaultValue = "18") String size){
            return productService.getAllProducts(page, size);
    }

    // Cập nhật thông tin sản phẩm qua id
    @PutMapping("products/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> updateProductById(@RequestParam Integer productLineId,
    @PathVariable Integer id,@RequestBody CProducts product){
        return productService.updateProductById(productLineId, id, product);
    }

    // Thêm mới sản phẩm
    @PostMapping("products")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> createProduct(@RequestParam Integer productLineId,
    @RequestBody CProducts product){
        return productService.createProduct(productLineId, product);
    }

    // Xoá thông tin 1 sản phẩm qua id
    @DeleteMapping("products/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> deleteProductById(@PathVariable Integer id){
        return productService.deleteProductById(id);
    }

    // Lấy số lượng sản phẩm
    @GetMapping("products/number")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> getNumberProduct(){
        return productService.getNumberProduct();
    }
}
