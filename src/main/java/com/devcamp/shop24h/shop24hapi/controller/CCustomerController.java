package com.devcamp.shop24h.shop24hapi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.shop24hapi.model.CCustomers;
import com.devcamp.shop24h.shop24hapi.payload.request.LoginRequest;
import com.devcamp.shop24h.shop24hapi.payload.request.SignupRequest;
import com.devcamp.shop24h.shop24hapi.service.CCustomerService;

@RestController
@RequestMapping("/shop24h/")
@CrossOrigin
public class CCustomerController {
    @Autowired
    CCustomerService customerService;
    // ================== PUBLIC =====================
    // Kiểm tra khả dụng đăng nhập người dùng
    @GetMapping("api/noauth/customers")
    public ResponseEntity<?> checkCustomerAvailableByRequest(HttpServletRequest request){
        return customerService.checkCustomerAvailableByRequest(request);
    }

    // Kiểm tra thông tin khách hàng qua phone number/ có thì gửi thông tin khách hàng về
    //  Không có thì tạo mới khách hàng và trả thông tin KH về
    @PostMapping("api/noauth/customers")
    public ResponseEntity<?> checkCustomer(@Valid @RequestBody CCustomers pCustomer){
        return customerService.checkCustomer(pCustomer);
    }

    // ================== LOGIN AND REGISTER ===========================

    // Đăng nhập vào hệ thống nhận mã jwt.
    @PostMapping("api/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){
        return customerService.authenticateUser(loginRequest);
    }


    // Đăng kí người dùng mới
    @PostMapping("api/noauth/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest){
        return customerService.registerUser(signUpRequest);
    }

    // ============================= MỤC DÀNH CHO QUYỀN MANAGER ===================================

    //lấy toàn bộ danh sách khách hàng
    @GetMapping("customers")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<List<CCustomers>> getAllCustomers(
        @RequestParam(defaultValue = "0") String page, 
        @RequestParam (defaultValue = "10") String size){
            return customerService.getAllCustomers(page, size);
        }

    // Lấy thông tin khách hàng qua id;
    @GetMapping("customers/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<CCustomers> getCustomerById (@PathVariable Integer id){
        return customerService.getCustomerById(id);
    }

    // Cập nhật thông tin khách hàng qua id
    @PutMapping("customers/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> updateCustomerById(@PathVariable Integer id, 
        @Valid @RequestBody CCustomers pCustomer,
        @RequestParam Long roleId){

        return customerService.updateCustomerById(id, pCustomer, roleId);
            }

    // Thêm mới khách hàng
    @PostMapping("customers")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody CCustomers pCustomer,
        @RequestParam Long roleId){

        return customerService.createCustomer(pCustomer, roleId);
    }
    
    //delete Khách hàng by id
    @DeleteMapping("customers/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable Integer id) {
        return customerService.deleteCustomerById(id);
    }

    // ===================== MỤC DÀNH CHO KHÁCH HÀNG (CUSTOMER ROLE)==========================

    //Cập nhật người dùng qua mã token trong request
    @PutMapping("customers/profile")
    @PreAuthorize("hasRole('MANAGER') or hasRole('CUSTOMER')")
    public ResponseEntity<?> updateCustomerByRequest(HttpServletRequest request,@Valid @RequestBody CCustomers pCustomer ){
        return customerService.updateCustomerByRequest(request, pCustomer);
    }

    //Cập nhật mật khẩu người dùng
    @PutMapping("customers/password")
    @PreAuthorize("hasRole('MANAGER') or hasRole('CUSTOMER')")
    public ResponseEntity<String> updateCustomerPasswordByRequest(HttpServletRequest request,
    @RequestParam String oldPassword,@RequestParam String newPassword){
        return customerService.updateCustomerPasswordByRequest(request, oldPassword, newPassword);
    }

}
